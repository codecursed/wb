package main

import (
	"fmt"
)

// Написать быструю сортировку встроенными методами языка.

func main() {
	a := []int{2, 45, 6, 19, 3, 7}
	//sort.Ints(a) // sort.Ints uses quicksort 229: https://cs.opensource.google/go/go/+/refs/tags/go1.17:src/sort/sort.go;drc=refs%2Ftags%2Fgo1.17;l=229
	fmt.Println(quickSort(a))
	fmt.Println(a)
}

func quickSort(a []int) []int {
	if len(a) <= 1 {
		return a
	}

	left, right := 0, len(a)-1
	flat := len(a) / 2

	a[flat], a[right] = a[right], a[flat]

	for i, _ := range a {
		if a[i] < a[right] {
			a[i], a[left] = a[left], a[i]
			left++
		}
	}
	a[left], a[right] = a[right], a[left]

	quickSort(a[:left])
	quickSort(a[left+1:])
	return a
}
