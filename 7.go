package main

import (
	"fmt"
	"sync"
)

// Реализовать конкурентную запись в map.

type MuMap struct {
	mu     sync.RWMutex //mu     *sync.RWMutex
	buffer map[int]string
}

func Create() *MuMap {
	return &MuMap{
		//mu:     &sync.RWMutex{},
		buffer: make(map[int]string),
	}
}

func (mMap *MuMap) Count() int {
	mMap.mu.RLock()
	defer mMap.mu.RUnlock()
	return len(mMap.buffer)
}

func (mMap *MuMap) SetValue(key int, value string) {
	mMap.mu.Lock()
	defer mMap.mu.Unlock()
	mMap.buffer[key] = value
}

func (mMap *MuMap) GetValue(key int) string {
	mMap.mu.RLock()
	defer mMap.mu.RUnlock()
	if len(mMap.buffer) > 0 {
		return mMap.buffer[key]
	}
	return ""
}

func main() {
	m := Create()
	wg := &sync.WaitGroup{}

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func(inc int, m *MuMap, wg *sync.WaitGroup) {
			defer wg.Done()
			m.SetValue(inc, "Example")
		}(i, m, wg)
	}
	wg.Wait()
	fmt.Println(m.buffer)
}
