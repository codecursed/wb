package main

import "fmt"

func someAction(v []int8, b int8) {
	v[0] = 100
	v = append(v, b)
}

func main() {
	var a = []int8{1, 2, 3, 4, 5}
	someAction(a, 6)
	fmt.Println(a)
}

/*	Что выведет данная программа и почему?
	[100 2 3 4 5], так как внутри функции создается копия объекта v, который указывает
	на ту же ячейку памяти, что и объект a из мейна. Когда мы подставляем в нулевой элемент
	в копию, то изменяем элемент непосредственно по адресу, а при использовании встроенного метода append
	мы превышаем объем памяти объекта, и, чтобы не допустить утечки, меняется адрес у локальной копии.
*/
