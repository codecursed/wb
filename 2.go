package main

import (
	"fmt"
	"sync"
)

// Написать программу, которая конкурентно
// рассчитает значение квадратов значений взятых
// из массива (2,4,6,8,10) и выведет их квадраты в stdout.

func main() {
	wg := &sync.WaitGroup{}
	arrayofInt := []int{2, 4, 6, 8, 10}
	for i := 0; i < len(arrayofInt); i++ {
		wg.Add(1)
		go func(num int, wg *sync.WaitGroup) {
			defer wg.Done()
			fmt.Printf("%d\n", num*num)
		}(arrayofInt[i], wg)
	}
	wg.Wait()
}
