package main

import (
	"fmt"
	"math"
)

// Дана последовательность температурных колебаний (-25.4, -27.0 13.0, 19.0, 15.5, 24.5, -21.0, 32.5).
// Объединить данный значения в группы с шагом в 10 градусов.
// Последовательность в подмножностве не важна.
// Пример: (-20:{-25.0, -27.0, -21.0}, 10:{13.0, 19.0, 15.5}, 20: {24.5}, etc)

func main() {
	arr := []float64{-20, -25.0, -27.0, -21.0, 10, 13.0, 19.0, 15.5, 20, 24.5, 34.2}
	result := make(map[float64][]float64)

	for i := 0; i < len(arr); i++ {
		value := math.Round(arr[i])   // ex: 34.2 -> 34
		valueShift := int(value) % 10 //   	 34 % 10 -> 4, map[30] = append(map, 34.2)
		result[value-float64(valueShift)] = append(result[value-float64(valueShift)], arr[i])
	}
	fmt.Println(result)
}
