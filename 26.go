package main

import "fmt"

// Написать программу, которая переворачивает строку. Символы могут быть unicode.

func reverseString1(s string) string {
	reverted := []int32(s) // unicode is int32

	for i := 0; i < len(reverted)/2; i++ {
		reverted[i], reverted[len(reverted)-1-i] = reverted[len(reverted)-1-i], reverted[i]
	}
	return string(reverted) // back to UTF-8
}

func reverseString2(s string) string {
	reverted := make([]int32, len(s))

	for i, element := range s {
		reverted[len(s)-i-1] = int32(element)
	}
	return string(reverted)
}

/*
func reverseString3(s string) string {
	reverted := make([]int32, 0)

	for i := len(s) - 1; i >= 0; i-- {
		//reverted = append(reverted, int32(s[i]))
		fmt.Printf("%s\n", int32(s[i]))
	}
	return string(reverted)
}
*/

func main() {
	emojis := "🤎🖤🤍💜💙💚💛🧡"
	fmt.Println(reverseString1(emojis))
	fmt.Println(reverseString2(emojis))
	//fmt.Println(reverseString3(emojis))
}
