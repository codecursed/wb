package main

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

// Написать программу, которая будет последовательно писать значения в канал, а с другой стороны канала — читать.
// По истечению N секунд программа должна завершиться.

func main() {
	var nSec time.Duration
	wg := &sync.WaitGroup{}
	fmt.Print("Enter time in seconds: ")
	fmt.Scan(&nSec)
	wg.Add(1)
	go rwTimer(wg, nSec)
	wg.Wait()
}

func rwTimer(wg *sync.WaitGroup, n time.Duration) {
	defer wg.Done()
	ctx, _ := context.WithTimeout(context.Background(), time.Duration(n*time.Second))
	channel := make(chan interface{})

	go func(channel chan interface{}, ctx context.Context) {
		arr := []interface{}{22, "catch", "cat", 19, "goes"}
		ticker := time.NewTicker(time.Second)
		for {
			select {
			case <-ticker.C:
				channel <- arr[rand.Intn(len(arr))]
			case <-ctx.Done():
				ticker.Stop()
				return
			}
		}
	}(channel, ctx)

	go func(channel chan interface{}, ctx context.Context) {
		for {
			select {
			case tmp := <-channel:
				fmt.Printf("Read value %v from channel\n", tmp)
			case <-ctx.Done():
				return
			}
		}
	}(channel, ctx)
	<-ctx.Done()
}
