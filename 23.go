package main

import (
	"fmt"
	"sort"
)

// Написать бинарный поиск встроенными методами языка.

func main() {
	array := []int{1, 2, 9, 5, 31, 45, 68, 4, 12}
	//fmt.Printf("%v\n", binarySearch(68, array))
	key := 5
	sort.Ints(array)
	i := sort.SearchInts(array, key)
	if i < len(array) && array[i] == key {
		fmt.Printf("Value %d found at index %d\n", key, i)
	} else {
		fmt.Printf("Value not found.\n")
	}
}

func binarySearch(key int, a []int) bool {
	left := 0
	right := len(a) - 1

	for left <= right {
		split := (left + right) / 2
		if a[split] < key {
			left = split + 1
		} else {
			right = split - 1
		}
	}

	if left == len(a) || a[left] != key {
		return false
	}
	return true
}
