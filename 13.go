package main

import (
	"fmt"
	"sync"
)

func main() {
	wg := sync.WaitGroup{} //wg := &sync.WaitGroup{} - нужно
	for i := 0; i < 5; i++ {
		wg.Add(1)
		go func(wg sync.WaitGroup, i int) { //go func(wg *sync.WaitGroup, i int) {
			fmt.Println(i)
			wg.Done()
		}(wg, i)
	}
	wg.Wait()
	fmt.Println("exit")
}

/*
	Чем завершится данная программа?
	В каждую горутину передаётся копия wg, из-за чего мы не можем отслеживать количество выполненных горутин
*/
