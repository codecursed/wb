package main

import (
	"fmt"
	"math/rand"
)

// Написать конвейер чисел. Даны 2 канала - в первый пишутся числа из массива,
// во второй пишется результат операции 2*x, после чего данные выводятся в stdout.

func input(in chan int) {
	defer close(in)
	for i := 0; i < rand.Intn(36); i++ {
		in <- i
	}
}

func double(in chan int, out chan int) {
	defer close(out)
	for i := range in {
		out <- 2 * i
	}
}

func output(out chan int) {
	for i := range out {
		fmt.Println(i)
	}
}

func main() {
	in, out := make(chan int), make(chan int)
	go input(in)
	go double(in, out)
	go output(out)
	fmt.Scanln()
}
