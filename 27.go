package main

import (
	"fmt"
	"strings"
)

// Написать программу, которая переворачивает слова в строке (snow dog sun - sun dog snow).

func main() {
	str := "snow dog sun"
	fmt.Printf("%s\n", reverseWords(str))
}

func reverseWords(str string) string {
	a := strings.Split(str, " ") // separates words by key (" ") and puts into array

	for i := 0; i < len(a)/2; i++ {
		a[i], a[len(a)-1-i] = a[len(a)-1-i], a[i]
	}
	return strings.Join(a, " ") // concatenate all elements from array and separates with " "
}
