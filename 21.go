package main

import (
	"fmt"
	"sync"
)

// Написать программу, которая в конкурентном виде читает элементы из массива в stdout.

type ConcurrecyArray struct {
	Wg *sync.WaitGroup
	Mu *sync.RWMutex
	A  []int
}

func (cArray *ConcurrecyArray) cArrayReader(index int) {
	defer cArray.Wg.Done()
	if index < len(cArray.A) {
		cArray.Mu.RLock()
		defer cArray.Mu.RUnlock()
		fmt.Printf("%d\n", cArray.A[index])
	}
}

func CreateCArray(wg *sync.WaitGroup, mu *sync.RWMutex, a []int) *ConcurrecyArray {
	return &ConcurrecyArray{
		Wg: wg,
		Mu: mu,
		A:  a,
	}
}

func main() {
	wg := &sync.WaitGroup{}
	mu := &sync.RWMutex{}
	array := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	cArray := CreateCArray(wg, mu, array)

	for i := range cArray.A {
		cArray.Wg.Add(1)
		go cArray.cArrayReader(i)
	}
	cArray.Wg.Wait()
}
