package main

import "fmt"

func main() {
	n := 0
	if true {
		n := 1
		n++
	}
	fmt.Println(n)
}

// Что выведет программа данная программа?
// Выведется нуль, так как блок с if true другая область видимости.
