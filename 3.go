package main

import (
	"sync"
)

// Дана последовательность чисел  (2,4,6,8,10) найти
// их сумму квадратов(2^2+3^2+4^2….) с использованием конкурентных вычислений.

func main() {
	arrayofInt := []int{2, 4, 6, 8, 10}
	wg := &sync.WaitGroup{}
	var sum int

	for i := 0; i < len(arrayofInt); i++ {
		wg.Add(1)
		go func(num int, wg *sync.WaitGroup) {
			defer wg.Done()
			sum += num * num
		}(arrayofInt[i], wg)
	}
	wg.Wait()
	println(sum)
}
