package main

import "fmt"

//Имеется последовательность строк - (cat, cat, dog, cat, tree) создать для нее собственное множество.

func main() {
	m := make(map[string]int)
	a := []string{"cat", "cat", "dog", "cat", "tree"}

	for i := 0; i < len(a); i++ {
		if _, ok := m[a[i]]; !ok {
			m[a[i]] = 1
		} else {
			continue // we only trying to find unique elements for set
		}
	}

	set := make([]string, len(m))
	inc := 0
	for key, _ := range m {
		set[inc] = key
		inc++
	}
	fmt.Println(set)
}
