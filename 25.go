package main

import (
	"fmt"
	"sync"
)

// Написать свою структуру счетчик, которая будет инкрементировать и выводить значения в конкурентной среде.

type Counter struct {
	Mu    sync.RWMutex
	Count uint
}

func (c *Counter) Nullify() {
	c.Count = uint(0)
}

func (c *Counter) Add(num uint) {
	c.Mu.Lock()
	defer c.Mu.Unlock()
	c.Count += num
	if c.Count == 4294967294 { // capacity of uint32 - 1
		c.Nullify()
	}
	fmt.Printf("%d\n", c.Count)
}

func CreateCounter() *Counter {
	return &Counter{}
}

func Increment(c *Counter, wg *sync.WaitGroup) {
	defer wg.Done()
	c.Add(1)
}

func main() {
	wg := &sync.WaitGroup{}
	c := CreateCounter()

	for i := 0; i < 100; i++ {
		wg.Add(1)
		go Increment(c, wg)
	}
	wg.Wait()
}
