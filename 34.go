package main

import "fmt"

// Написать программу, которая проверяет, что все символы в строке уникальные.

func unique(s string) bool {
	symMap := make(map[rune]bool)
	for _, sym := range s {
		if _, ok := symMap[sym]; ok {
			return false
		} else {
			symMap[sym] = true
		}
	}
	return true
}

func main() {
	s := "hello"
	fmt.Println(unique(s))
}
