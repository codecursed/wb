package main

import "fmt"

var justString string

func someFunc() {
	v := createHugeString(1 << 10)
	justString += v[:100]
	fmt.Println(justString)
}

func main() {
	someFunc()
}

// функция дописана
func createHugeString(length int) string {
	justString = "What???"
	str := ""
	for i := 0; i < length; i++ {
		str += "Hey"
	}
	return str
}

/*
	К каким негативным последствиям может привести данный кусок кода и как это исправить?
	В данном куске кода имеется глобальная переменная, которую не рекомендуется использовать в
	контексте данного кода. Значения глобальных переменных могут изменять любые вызываемые функции,
	при этом программист может этого и не знать. Как пример - я добавил в глобальную переменную значение,
	не подразумевавшееся в условиях работы программы.
*/
