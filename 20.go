package main

import "fmt"

func main() {
	slice := []string{"a", "a"}

	func(slice []string) {
		slice = append(slice, "a")
		slice[0] = "b"
		slice[1] = "b"
		fmt.Print(slice)
	}(slice)
	fmt.Print(slice)
}

/*
	Какой результат выполнения данного кода и почему?
	Как и в 18-м задании, копия объекта slice перестает указывать на ту же область памяти, что и оригинал из-за
	того, что после append копии объекта необходимо больше памяти => ей выделяется новая область (capacity становится
	вдвое больше).
	Поэтому stdout будет [b b a][a a]
*/
