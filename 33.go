package main

import (
	"fmt"
	"math/rand"
)

// Даны 2 канала - в первый пишутся рандомные числа после чего они проверяются
// на четность и отправляются во второй канал. Результаты работы из второго канала пишутся в stdout.

func genNum(in chan<- int) {
	defer close(in)
	for i := 0; i < 10; i++ {
		if num := rand.Intn(128); num%2 == 0 {
			in <- num
		}
	}
}

func receiver(in chan int, out chan int) {
	defer close(out)
	for val := range in {
		out <- val
	}

}

func main() {
	in := make(chan int)
	out := make(chan int)

	go genNum(in)
	go receiver(in, out)

	for num := range out {
		fmt.Println(num)
	}
}
