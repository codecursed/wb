package main

import (
	"fmt"
	"math"
)

// Написать программу нахождения расстояния между 2 точками,
// которые представление в виде структуры Point с инкапсулированными параметрами x,y и конструктором.

type Point struct {
	x int
	y int
}

func NewSegment(_x int, _y int) *Point {
	return &Point{
		x: _x,
		y: _y,
	}
}

func Distance(p1 *Point, p2 *Point) float64 {
	a := float64(p1.x - p2.x)
	b := float64(p1.y - p2.y)
	return math.Sqrt(a*a + b*b)
}

func main() {
	p1 := NewSegment(5, 4)
	p2 := NewSegment(3, 4)
	fmt.Printf("%v\n", Distance(p1, p2))
}
