package main

import (
	"fmt"
	"reflect"
)

// Написать программу, которая в рантайме способна определить
// тип переменной — int, string, bool, channel из переменной типа interface{}.

//using reflect library

func firstRuntimeDet(valueType interface{}) {
	fmt.Println(reflect.TypeOf(valueType))
}

func secondRuntimeDet(valueType interface{}) {
	fmt.Printf("%T\n", valueType)
}

func thirdRuntimeDet(valueType interface{}) {
	fmt.Println(reflect.ValueOf(valueType).Type())
}

func fourthRuntimeDet(valueType interface{}) {
	switch v := valueType.(type) {
	case int:
		fmt.Println("int")
	case string:
		fmt.Println("string")
	case bool:
		fmt.Println("bool")
	case chan int:
		fmt.Println("chan int")
	case chan string:
		fmt.Println("chan string")
	case chan bool:
		fmt.Println("chan bool")
	default:
		fmt.Printf("unknown type, but it's %T\n", v)
	}
}

func main() {
	t := byte(1)
	a := []interface{}{2, "Byte))", t}
	for _, val := range a {
		fourthRuntimeDet(val)
	}
}
