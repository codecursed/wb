package main

import (
	"fmt"
	"sync"
)

// Реализовать набор из N воркеров, которые читают из канала произвольные данные и выводят в stdout.
// Данные в канал пишутся из главного потока.
// Необходима возможность выбора кол-во воркеров при старте, а также способ завершения работы всех воркеров.

func workers(in chan interface{}, wg *sync.WaitGroup, orderNum int) {
	defer wg.Done()
	value := <-in
	fmt.Printf("Worker №%d with value %v and type %T\n", orderNum, value, value)
}

func main() {
	var n int
	wg := &sync.WaitGroup{}
	channel := make(chan interface{})
	fmt.Print("Enter amount of workers: ")
	fmt.Scanf("%d\n", &n)
	wg.Add(n)
	for i := 0; i < n; i++ {
		go workers(channel, wg, i)
	}

	valueArray := []interface{}{"dasd", 123, "asddddas", 2929, "xsxxsxdxdxd"}
	for i := 0; i < n; i++ {
		if i < len(valueArray) {
			channel <- valueArray[i]
		} else {
			channel <- i
		}

	}
	close(channel)
}
