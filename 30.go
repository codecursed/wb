package main

import "fmt"

// Удалить i-ый элемент из слайса.

const (
	DELETE_INDEX = 2
)

func main() {
	slice := []int{1, 2, 3, 4, 5, 6}
	//fmt.Printf("%d\n", deleteWithOrder(slice, DELETE_INDEX))
	fmt.Printf("%d\n", deleteWithoutOrder(slice, DELETE_INDEX))
}

// slow version for saving order - shifting all values
func deleteWithOrder(slice []int, i int) []int {
	return append(slice[:i], slice[i+1:]...)
}

// fast version with swap last element
func deleteWithoutOrder(slice []int, i int) []int {
	slice[i] = slice[len(slice)-1]
	return slice[:len(slice)-1]
}
