package main

import "fmt"

type Human struct {
	Age  uint
	Name string
}

type Action struct {
	*Human
	speak Speak
}

type Speak struct {
	vol    Volume
	phrase string
}

func (s *Speak) tellPhrase(words string) {
	s.phrase = words
}

type Volume struct {
	maxLoud  dB
	minQuiet dB
}

func (v *Volume) SetVolumeRange(min dB, max dB) {
	v.minQuiet = min
	v.maxLoud = max
}

type dB int

func main() {
	act := &Action{
		Human: &Human{25, "Anton Antonov"},
	}
	act.speak.tellPhrase("Sayonara!")
	vol := &Volume{}
	vol.SetVolumeRange(1, 4)
	fmt.Printf("%s says '%s' with volume range (%d, %d)\n",
		act.Human.Name, act.speak.phrase,
		vol.minQuiet, vol.maxLoud)
	/*
		Anton Antonov says 'Sayonara!' with volume range (1, 4)
	*/
}
