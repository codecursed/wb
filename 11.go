package main

import (
	"fmt"
)

// Написать пересечение двух неупорядоченных массивов.

func arrIntersectionN2(firstArray []int, secondArray []int) (result []int) {
	m := make(map[int]bool)

	for _, i := range firstArray {
		m[i] = true
	}

	for _, i := range secondArray {
		if _, ok := m[i]; ok {
			result = append(result, i)
		}
	}
	return
}

func max(a, b int) int {
	if a >= b {
		return a
	}
	return b
}

func arrIntersectionHash(firstArray []int, secondArray []int) []int {
	result := make([]int, 0)
	hash := make(map[int]int)
	length := max(len(firstArray), len(secondArray))

	for i := 0; i < length; i++ {
		if i < len(firstArray) {
			if _, ok := hash[firstArray[i]]; !ok {
				hash[firstArray[i]] = 1
			} else {
				hash[firstArray[i]]++
			}
		}
		if i < len(secondArray) {
			if _, ok := hash[secondArray[i]]; !ok {
				hash[secondArray[i]] = 1
			} else {
				hash[secondArray[i]]++
			}
		}

	}

	for key, value := range hash {
		if value > 1 {
			result = append(result, key)
		}
	}
	return result
}

func main() {

	var a = []int{1, 4, 7, 14, 128, 48}
	var b = []int{2, 8, 4, 48}

	fmt.Printf("%d\n%d\n%d\n", a, b, arrIntersectionN2(a, b))
	fmt.Printf("------------\n")
	fmt.Printf("%d\n%d\n%d\n", a, b, arrIntersectionHash(a, b))
}
