package main

import "fmt"

// Создать слайс с предварительно выделенными 100 элементами.

func main() {
	slice := make([]interface{}, 100)
	fmt.Println(cap(slice), len(slice))
}
