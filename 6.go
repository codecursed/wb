package main

import (
	"context"
	"fmt"
	"sync"
	"time"
)

const (
	shortTime = 5 * time.Second
)

// Какие существуют способы остановить выполнения горутины? Написать примеры использования.

func main() {
	//firstEx()
	//secondEx()
	//thirdEx()
	//fourthEx()
	fifthEx()

	//stopGr <- struct{}{} // send empty struct for first example

	fmt.Println("\nExited!")
}

func firstEx() {
	// first example
	stopGr := make(chan struct{})
	wg := &sync.WaitGroup{}
	go func() {
		defer wg.Done()
		wg.Add(1)
		for {
			select {
			case <-stopGr:
				return
			}
		}
	}()
}

// simple stop chan
func secondEx() {
	stopGr := make(chan struct{})
	wg := &sync.WaitGroup{}
	// second example
	go func() {
		defer wg.Done()
		wg.Add(1)
		close(stopGr)
		fmt.Println("Invisible me") // unreachable line
	}()
}

// context deadline
func thirdEx() {
	wg := &sync.WaitGroup{}
	d := time.Now().Add(shortTime)
	ctx, cancel := context.WithDeadline(context.Background(), d)
	defer cancel()
	wg.Add(1)
	go func(ctx context.Context, wg *sync.WaitGroup) {
		defer wg.Done()
		for {
			select {
			case <-ctx.Done():
				fmt.Println("exiting: <-ctx.Done()")
				return
			default:
				fmt.Println("Worker here")
			}
		}
	}(ctx, wg)
	wg.Wait()
}

// context timeout
func fourthEx() {
	wg := &sync.WaitGroup{}
	ctx, cancel := context.WithTimeout(context.Background(), shortTime)
	defer cancel()
	wg.Add(1)
	go func(ctx context.Context, wg *sync.WaitGroup) {
		defer wg.Done()
		for {
			select {
			case <-ctx.Done():
				fmt.Println("exiting: <-ctx.Done()")
				return
			default:
				fmt.Println("Worker here")
			}
		}
	}(ctx, wg)
	wg.Wait()
}

// context cancel
func fifthEx() {
	channel := make(chan int)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	child := func(ctx context.Context) <-chan int {
		inc := 1
		go func() {
			for {
				select {
				case <-ctx.Done():
					return
				case channel <- inc:
					inc++
					fmt.Print("Hey")
				}
			}
		}()
		return channel
	}

	for i := range child(ctx) {
		if i == 5 {
			break
		}
	}
}
