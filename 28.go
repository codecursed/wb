package main

import (
	"fmt"
	"strconv"
)

// Реализовать паттерн адаптер на любом примере.

type Client struct {
	Num int
}

func (c *Client) IntReceiver(d Data) {
	result := d.getData()
	fmt.Printf("Int '%d' received\n", result)
}

type Data interface {
	getData() interface{}
}

type IntService struct {
	Num int
}

func (is *IntService) getData() interface{} {
	fmt.Printf("Get %d from Int Service\n", is.Num)
	return is.Num
}

type StringService struct {
	Str string
}

func (ss *StringService) StrSender() string {
	return ss.Str
}

type StringServiceAdapter struct {
	CStr string
}

func (ssa *StringServiceAdapter) getData() interface{} {
	result, _ := strconv.Atoi(ssa.CStr)
	return result
}

func main() {
	client := &Client{}
	intService := &IntService{
		Num: 128,
	}

	client.IntReceiver(intService)

	stringService := &StringService{
		Str: "129",
	}
	sSAdapter := &StringServiceAdapter{
		CStr: stringService.Str,
	}
	client.IntReceiver(sSAdapter)
}
