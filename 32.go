package main

import "time"

// Написать собственную функцию Sleep.

func Snooze(t time.Duration) {
	<-time.After(time.Second * t) // time after method returning <-chan time.Time
}

func main() {
	Snooze(5)
}
