package main

import (
	"fmt"
)

// Поменять местами два числа без создания временной переменной.

func firstReplace(a int, b int) (int, int) {
	return b, a
}

func secondReplace(a int, b int) (int, int) {
	a = a + b // analogue version is (a = a - b) and etc.
	b = a - b
	a = a - b
	return a, b
}

func thirdReplace(a int, b int) (int, int) {
	a ^= b      // example: 5 ^= 2 -> 101 ^ 10 = 111 = 7
	b ^= a      // 2 ^= 7 -> 10 ^ 111 = 101 = 5
	a ^= b      // 7 ^= 5 -> 111 ^ 101 = 10 = 2
	return a, b // 2, 5
}

func fourthReplace(a int, b int) (int, int) {
	a = a * b
	b = a / b
	a = a / b
	return a, b
}

func fifthReplace(a int, b int) (int, int) {
	// will implement later using by digit bitwise
}

func main() {
	a, b := 5, 2
	fmt.Println(fourthReplace(a, b))
}
